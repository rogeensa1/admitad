<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;

class insertProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product=new Product();
        $product->product_id=$this->data['@attributes']['id'];

        //because reponse not have same parameters
        if($this->data['@attributes']['available']==true){
            $product->available=1;
        }else{
            $product->available=0;
        }

        if(array_key_exists('categoryId', $this->data)){
            $product->category_id=$this->data['categoryId'];
        }else{
            $product->category_id=-1;
        }

        if(array_key_exists('currencyId', $this->data)){
            $product->currency_id=$this->data['currencyId'];
        }else{
            $product->currency_id='';
        }

        if(array_key_exists('description', $this->data)){
            $product->description=$this->data['description'];
        }else{
            $product->description='';
        }

        if(array_key_exists('modified_time', $this->data)){
            $product->modeified_date=date("Y-m-d", $this->data['modified_time']);
        }else{
            $product->modeified_date=date('Y-m-d');
        }

        if(array_key_exists('name', $this->data)){
            $product->name=$this->data['name'];
        }else{
            $product->name='';
        }

        if(array_key_exists('oldprice', $this->data)){
            $product->old_price=$this->data['oldprice'];
        }else{
            $product->old_price=0;
        }

        if(array_key_exists('price', $this->data)){
            $product->price=$this->data['price'];
        }else{
            $product->price=0;
        }


        if(array_key_exists('picture', $this->data)){
            $product->picture=$this->data['picture'];
        }else{
            $product->picture='';
        }

        if(array_key_exists('url', $this->data)){
            $product->url=$this->data['url'];
        }else{
            $product->url='';
        }

        if(array_key_exists('vendor', $this->data)){
            $product->vendor=$this->data['vendor'];
        }else{
            $product->vendor='';
        }
        
        $product->save();
    }
}
