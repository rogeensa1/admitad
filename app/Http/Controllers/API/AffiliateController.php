<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Admitad\Api\Api;
use Illuminate\Support\Facades\Http;
use App\jobs\xmlProductsParse;
use Config;

class AffiliateController extends Controller
{

    public function listPrograms()
    {
      $clientId = Config::get('admitad.options.client_id');
      $clientPassword = Config::get('admitad.options.client_password');
      $username = Config::get('admitad.options.username');
      $password = Config::get('admitad.options.password');
      $scope = Config::get('admitad.options.scope');

      $api = new Api();
      $response = $api->authorizeByPassword($clientId, $clientPassword, $scope, $username, $password);
      $result = $response->getResult(); // or $response->getArrayResult();

      $token = $result['access_token'];
      
      //1325397 is add space unimart deals id https://api.admitad.com/websites/v2/
      $affiliateApi = new Api($token);
      $data = $affiliateApi->get('/advcampaigns/website/1325397/')->getArrayResult();

      $programms = [];

      //dd($data['results']);

      foreach ($data['results'] as $key => $value) {

          $programms[$key]['name'] = $value['name'];
          $programms[$key]['image'] = $value['image'];
          
          if($value['allow_deeplink'] == true && $value['connection_status'] == 'active'){

            $url='/deeplink/1325397/advcampaign/'.$value['id'].'/?subid=Nike&subid1=white_sneakers&subid2=40sale&ulp=http%3A%2F%2Fadmitad.com%2Fpost%2F250618%2F&ulp=http%3A%2F%2Fadmitad.com%2Fpost%2F220658%2F';
            $deepLink = $affiliateApi->get($url)->getArrayResult();  
            $programms[$key]['deepLink'] = $deepLink[0];
              
          }else{
              $programms[$key]['deepLink'] = null;
          }

        
          //get prgramms products
          if (isset($value['products_xml_link'])) {
              dispatch( new xmlProductsParse($value['products_xml_link']));
          }
        
      }

      return response()->json([
                     'code'=>200,
                     'programs' => $programms,
                  ]);

    }

}
