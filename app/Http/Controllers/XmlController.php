<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\jobs\insertProducts;


class XmlController extends Controller
{

    public function index(){
        
        //$xmlString = file_get_contents(public_path('xml/products.xml'));
        $xmlString = file_get_contents('http://export.admitad.com/webmaster/websites/1325397/products/export_adv_products/?user=unimart&code=qeasg18yei&feed_id=1950&format=xml');
        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        //dd($phpArray['shop']);

        $products=$phpArray['shop']['offers']['offer'];

        foreach($products as $product){
            dispatch( new insertProducts($product));
        }
        
    }

}

