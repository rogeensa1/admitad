<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\XmlController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('xmlReader', [XmlController::class, 'index']);
Route::get('xmlSeparate', [XmlController::class, 'separateXmlFile']);


Route::get('/{path?}', function () {
    return view('home');
})->where('path','([A-z\d\-\/_.]+)?');
