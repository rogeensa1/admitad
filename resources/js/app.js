require('./bootstrap');
import Vue from 'vue';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import Home from './components/Home.vue';

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

new Vue({
    el: '#app',

    render: h => h(Home)
  });
