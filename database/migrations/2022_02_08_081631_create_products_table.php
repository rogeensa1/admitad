<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id');
            $table->bigInteger('category_id');
            $table->tinyInteger('available');
            $table->string('name');
            $table->string('currency_id');
            $table->text('description');
            $table->text('url');
            $table->decimal('old_price');
            $table->decimal('price');
            $table->text('picture');
            $table->text('vendor');
            $table->date('modeified_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
